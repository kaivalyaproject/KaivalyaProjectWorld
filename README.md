# KaivalyaProjectWorld

KaivalyaProjectWorld is the board where we discuss main topics of KaivalyaProject.  
For example here you can find discussion about [server maintenance](https://gitlab.com/kaivalyaproject/KaivalyaProjectWorld/issues/1), [proposes of collaboration](https://gitlab.com/kaivalyaproject/KaivalyaProjectWorld/issues/3).  
You can also find [documents](https://gitlab.com/kaivalyaproject/KaivalyaProjectWorld/tree/master/Docs) of different kind (like the [Open Letter to students](https://gitlab.com/kaivalyaproject/KaivalyaProjectWorld/blob/master/Docs/issues/issue%233/Open_letter_to_students_en.md)).


This board is open to everyone to let you comment and improve ideas shared in issues.  
Issue submission should be enabled for staff member only but we want you to be able to talk about your ideas, so we ask you to please do not abuse of this board.  
If you want to submit an idea about KP internals just create an issue, we appreciate.  
In the future we will use an utility to manage this kind of stuff (discussion, sharing ideas, ...). Till then we will use GitLab issue board to manage our projects.