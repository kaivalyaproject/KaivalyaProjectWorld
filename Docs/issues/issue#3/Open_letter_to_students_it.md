<div id="table-of-contents">
<h2>Table of Contents</h2>
<div id="text-table-of-contents">
<ul>
<li><a href="#sec-1">1. Lettera aperta agli studenti di tutto il mondo [IT]</a>
<ul>
<li><a href="#sec-1-1">1.1. Cos'&egrave; KaivalyaProject</a></li>
<li><a href="#sec-1-2">1.2. Perch&egrave; questa lettera &egrave; rivolta agli studenti</a></li>
</ul>
</li>
</ul>
</div>
</div>

# Lettera aperta agli studenti di tutto il mondo [IT]<a id="sec-1" name="sec-1"></a>

A: studenti di tutto il mondo  
Da: KaivalyaProject (@lucabertoni) - kaivalyaproject at autistici.org  
Oggetto: KaivalyaProject cerca collaboratori nelle scuole  

Buongiorno,  
sono qui oggi per scrivere questa lettera a tutti gli studenti in giro per il mondo per invitarli a collaborare a KaivalyaProject.  
Nel dettaglio stiamo cercando collaboratori da inserire nello staff che ci possano aiutare in diverse parti del progetto. &Egrave; per il momento necessario individuare delle figure per le seguenti aree:  
-   computer grafica  
    (vedi: [issue#2](https://gitlab.com/kaivalyaproject/KaivalyaProjectWorld/issues/2))
-   gestione repository
-   programmazione

Al momento della stesura di questa lettera il progetto sta ancora prendendo vita.  
Attualmente stiamo lavorando per costruire un sito web che definisca l'idea che permea il progetto, da usare poi come punto di riferimento per i progetti che svilupperemo in futuro.  
Vediamo ora cosa &egrave; KaivalyaProject e perch&egrave; questa lettera &egrave; rivolta agli studenti.  

## Cos'&egrave; KaivalyaProject<a id="sec-1-1" name="sec-1-1"></a>

KaivalyaProject &egrave; una piattaforma all'interno della quale sar&agrave; possibile trovare software liberi (free sofware; free as freedom, vedi: [significato free software](https://www.gnu.org/philosophy/free-sw.html)).  
L'idea consiste nel portare sul mercato software (liberi) che possano aiutare le persone nel focalizzarsi su ci&ograve; di cui necessitano senza perdere tempo in preoccupazioni che riguardo la privacy, l'usabilit&agrave; e quelle cose che diamo per scontato siano implementate nei 
software che utilizziamo ogni giorno (i.e. applicazioni per prendere note, calendario, ecc.).

## Perch&egrave; questa lettera &egrave; rivolta agli studenti<a id="sec-1-2" name="sec-1-2"></a>

Da studente ho sempre cercato progetti che potessero aiutarmi a migliorare le mie capacit&agrave; nello sviluppo di sofware, nella gestione dei progetti etc.  
Questo &egrave; ci&ograve; che vorrei condividere con coloro che contribuiranno al progetto: conoscenza. In futuro saremo sicuramente contenti di poter ringraziare tutti coloro che ci hanno aiutati distribuendo parte delle donazioni effettuate a KP.  

Invito quindi gli studenti in giro per il mondo interessati a contribuire a KaivalyaProject a contattarci all'indirizzo mail: kaivalyaproject at autistici.org (sostituire 'at' con il simbolo della chiocciola).  

Grazie,  
Luca Bertoni e lo staff di KP.
