<div id="table-of-contents">
<h2>Table of Contents</h2>
<div id="text-table-of-contents">
<ul>
<li><a href="#sec-1">1. Open letter to students around the world [EN]</a>
<ul>
<li><a href="#sec-1-1">1.1. About KaivalyaProject</a></li>
<li><a href="#sec-1-2">1.2. Why students would care about KaivalyaProject</a></li>
</ul>
</li>
</ul>
</div>
</div>

# Open letter to students around the world [EN]<a id="sec-1" name="sec-1"></a>

To: all students around the world  
From: KaivalyaProject (@lucabertoni) - kaivalyaproject at autistici.org  
Subject: KaivalyaProject looking for collaborators in schools  

Goodmorning,  
today I am here writing this open letter to all students around the world to invite them collaborate to KaivalyaProject.  
In details we are looking for internal collaborators which could help in different fields of the project. Actually we need to cover following areas:  
-   computer graphics
    (more info at: [issue#2](https://gitlab.com/kaivalyaproject/KaivalyaProjectWorld/issues/2))
-   repository management
-   programming

At the moment of writing of this letter the project is still coming to life.  
We are actually working to build a website which could define the idea behind the project and as a board used to distribute projects we are going to develop.  
So let's take a deeper look into what KaivalyaProject is and why students would care about it.  

## About KaivalyaProject<a id="sec-1-1" name="sec-1-1"></a>

KaivalyaProject is, mainly, a platform where you will be able to find useful free software (free as freedom, see: [free software meaning](https://www.gnu.org/philosophy/free-sw.html)).  
The idea is to bring to the market (free) softwares which could help human beings in focusing on what they need without wasting time being worried about privacy, usability and all the kind of stuff we usally presume to be implemented in 
utilies we use in our daily routine (note-taking app, calendar, &#x2026;).

## Why students would care about KaivalyaProject<a id="sec-1-2" name="sec-1-2"></a>

As an ICT student I always looked for projects which could help me improve my skills in software development, project management etc.  
That is what actually you could receive being a collaborator of KP: knowledge. In the future we will be happy to thanks all collaborators distributing donations coming to KP.  

Now you should have an idea of what we are talking about, if you are interested in becoming a staff member mail us at: kaivalyaproject at autistici.org (replace at with at symbol).  

Thanks,  
Luca Bertoni and the KP staff.
